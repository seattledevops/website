# Welcome to the Seattle DevOps Meetup

Find this page at https://seattledevops.net/january2021/

January 27th, 2021

## Today’s Agenda:

4:00pm - Zoom event spun up; waiting 5 minutes to folks joining in\
4:05pm - Welcome (Jason Grimes)\
4:10pm - 4:15pm - COC and Hiring (Dave Nash)\
4:15pm - 4:45pm - Matty Stratton - What Got You Here Won’t Get You There\
4:45pm - 5:15pm - Sasha Rosenbaum - Mindset\
5:15pm - Wrap up

** Times are approximate

## Announcements:

**Attention: We are in need of sponsors to make and keep these events free for attendees.**

Email me at _jason dot grimes at gmail dot com_, subject **Seattle DevOps Meetup Sponsorship** and we can make time to connect or complete this form online - https://forms.gle/wureudFy9KK6vECMA

## Important Links:

[Seattle DevOps Meetup Code of Conduct](https://seattledevops.net/codeofconduct/)\
[Submit your speaking request](https://forms.gle/v3seEazumFFibxgn7)\
[Are you interested in sponsorship?](https://forms.gle/wureudFy9KK6vECMA)\
[We would love your feedback!](https://forms.gle/FVjzPzkqskohithW9)\
[Slack Invite(Valid Until 2/25/2021)](https://join.slack.com/t/seattle-devops/shared_invite/zt-lg6w792s-sowxkaLeSBUm7w79wO4wzg)


## [Next Month: February 24th, 2021 at 4pm - 5:30pm PT](https://seattledevops.net/posts/upcoming_events/20210224/)
Nell Shamrell-Harrington - It’s Only Hard Parts Now: Harnessing Community to Thrive in a World of Complexity\
Karthik Gaekwad - Once Upon a Time in Kubernetes Operator land…


