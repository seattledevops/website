# Welcome to the Seattle DevOps Meetup

Find this page at https://seattledevops.net/april2021/

April 28th, 2021

## Today’s Agenda:

4:00pm - Welcome + Networking \
4:10pm - 4:12pm - 2 minutes from our March Sponsor - Chef \
4:15pm - 4:45pm - Jason DeTiberus - Cluster API With a Bit of PXE Dust: bringing cloud-native declarative management of Kubernetes clusters into the datacenter\
4:45pm - 5:15pm - Mandy Riso - How to build once, deploy many using Azure DevOps multi-stage pipelines\
5:15pm - Wrap up
***
## Sponsors:

{{< figure src="/sponsors/square_Chef_Prorgess_SecondaryLogo_Stacked.png" alt="Chef -Progress" width="150px" >}} 
***
## Announcements:

**Attention: We are in need of sponsors to make and keep these events free for attendees.**

Email me at _jason dot grimes at gmail dot com_, subject **Seattle DevOps Meetup Sponsorship** and we can make time to connect or complete this form online - https://forms.gle/wureudFy9KK6vECMA

## Important Links:

[Seattle DevOps Meetup Code of Conduct](https://seattledevops.net/codeofconduct/)\
[Submit your speaking request](https://forms.gle/v3seEazumFFibxgn7)\
[Are you interested in sponsorship?](https://forms.gle/wureudFy9KK6vECMA)\
[We would love your feedback!](https://forms.gle/FVjzPzkqskohithW9)\
[Slack Invite(Valid Until 5/27/2021)](https://join.slack.com/t/seattle-devops/shared_invite/zt-pfo69l07-pBp~TxgoJyA8Ldaiaw2sxg)


## [ Next Month: May 26th, 2021 at 4pm - 5:30pm PT](https://seattledevops.net/posts/upcoming_events/20210526/) 
Jeffery Smith - TBD \
TBD - TBD


