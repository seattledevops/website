---
subtitle: ""
tags: ["event", "devops", "meetup", "Alina Anderson", "Julie Gunderson"]
---

## Wednesday, August 25th, 2021 - 4:00 PM to 5:30 PM PST

## [RSVP Here ](https://www.meetup.com/Seattle-DevOps-Meetup/events/277853467/)

***

## Sponsors:

{{< figure src="/sponsors/square_Chef_Prorgess_SecondaryLogo_Stacked.png" alt="Chef -Progress" width="150px" >}} 

***
## Agenda:

4:00pm - Welcome + Networking\
4:15pm - 4:45pm - Alina Anderson - Survival Guide: What I Learned From Putting 200 Developers On Call\
4:45pm - 5:15pm - Julie Gunderson - Failure is inevitable in modern, complex applications\
5:15pm - Wrap up

** Times are approximate

***

{{< figure src="/speakers/alina_anderson.jpg" alt="Alina Anderson" width="150px" >}} 
## [Alina Anderson](https://twitter.com/justmealina)
####
Alina is a Senior TPM, cat herding organizations through complex challenges at the intersection of humans and systems. Over the last six years, she has navigated on-call through pre-IPO hypergrowth to Enterprise scale. Alina is committed to giving back to the Seattle Devops community through Ada Developers Academy mentorship, co-organizing DevOps Days Seattle and volunteering on the King County crisis hotline.
### Survival Guide: What I Learned From Putting 200 Developers On Call

{{< youtube bzz1msMbBxY >}}

We want to live in a world where the development team who writes the code, also owns that code’s success...or failure, in production. Nothing incentivizes a team to ship better quality software than getting paged at 2am, but how do we do this? In this talk, you’ll learn some tips and tricks for easing less than enthusiastic development teams into on-call rotations, how SRE facilitates the transition to production code ownership and why SLOs are critical to your success.


***

{{< figure src="/speakers/julie_gund.jpg" alt="Julie Gunderson" width="150px" >}}
## [Julie Gunderson](https://twitter.com/Julie_Gund)
#### 

Julie Gunderson is a Sr. Reliability Advocate at Gremlin, where she works to further the adoption of Chaos Engineering principles and methodologies. Over the last seven years, Julie has been actively involved in the DevOps space and is passionate about helping individuals, teams, and organizations understand how to leverage best practices and develop amazing cultures. Julie is also a founding member of DevOpsDays Boise.
### Failure is inevitable in modern, complex applications

{{< youtube avlMfmDPvoA >}}

Failure is inevitable in modern, complex applications, but with Chaos Engineering, you can minimize the effect on your organization and customers. We’ll share why Chaos Engineering is a critical part of the software lifecycle and how you can implement it in your organization.
In this session we’ll cover the cost of incidents, where Chaos Engineering came from and why it’s important, how you can begin implementing Chaos Engineering in your organization, and we’ll share a couple case studies to highlight the real-world benefits of practicing it.