---
title: Discord

comments: false
---

Our Discord Server name is Seattle DevOps.\
This is fairly new and has significantly fewer users than the slack workspace.\
Currently there is no plan to link Discord and Slack.

To request an invite link DM @seattledevops on twitter