# Welcome to the Seattle DevOps Meetup

Find this page at https://seattledevops.net/august2021/

August 25th, 2021

## Today’s Agenda:

4:00pm - Welcome + Networking \
4:10pm - 4:12pm - 2 minutes from our August Sponsor - Chef/Progress \
4:15pm - 4:45pm - Alina Anderson - Survival Guide: What I Learned From Putting 200 Developers On Call\
4:45pm - 5:15pm - Julie Gunderson - Failure is inevitable in modern, complex applications\
5:25pm - Wrap up
***
## Sponsors:

{{< figure src="/sponsors/square_Chef_Prorgess_SecondaryLogo_Stacked.png" alt="Chef -Progress" width="150px" >}} 
***
## Announcements:

**Attention: We are in need of sponsors to make and keep these events free for attendees.**

Email me at _jason dot grimes at gmail dot com_, subject **Seattle DevOps Meetup Sponsorship** and we can make time to connect or complete this form online - https://forms.gle/wureudFy9KK6vECMA

## Important Links:

[Seattle DevOps Meetup Code of Conduct](https://seattledevops.net/codeofconduct/)\
[Submit your speaking request](https://forms.gle/v3seEazumFFibxgn7)\
[Are you interested in sponsorship?](https://forms.gle/wureudFy9KK6vECMA)\
[We would love your feedback!](https://forms.gle/FVjzPzkqskohithW9)\
[Slack Invite(Valid Until 8/27/2021)](https://join.slack.com/t/seattle-devops/shared_invite/zt-tlas4ccy-dBPAUL6DmpxEUzoLTCM6ZQ)


## [ Next Month: Sepetember 29th, 2021 at 4pm - 5:30pm PT](https://www.meetup.com/Seattle-DevOps-Meetup/events/XXXX/) 




