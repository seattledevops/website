# Welcome to the Seattle DevOps Meetup

Find this page at https://seattledevops.net/march2021/

March 31st, 2021

## Today’s Agenda:

4:00pm - Welcome + Networking \
4:10pm - 4:12pm - 2 minutes from our March Sponsor - TBD \
4:15pm - 4:45pm - Aaron Aldrich - Sticking Together while Staying Apart: Resilience in the Time of Global Pandemic\
4:45pm - 5:15pm - Quintessence Anx - Prevent Heroism: How to Work Today to Reduce Work Tomorrow\
5:15pm - Wrap up
***
## Sponsors:

{{< figure src="/sponsors/sdm_only_logo.jpg" alt="TBD" width="150px" >}} 
***
## Announcements:

**Attention: We are in need of sponsors to make and keep these events free for attendees.**

Email me at _jason dot grimes at gmail dot com_, subject **Seattle DevOps Meetup Sponsorship** and we can make time to connect or complete this form online - https://forms.gle/wureudFy9KK6vECMA

## Important Links:

[Seattle DevOps Meetup Code of Conduct](https://seattledevops.net/codeofconduct/)\
[Submit your speaking request](https://forms.gle/v3seEazumFFibxgn7)\
[Are you interested in sponsorship?](https://forms.gle/wureudFy9KK6vECMA)\
[We would love your feedback!](https://forms.gle/FVjzPzkqskohithW9)\
[Slack Invite(Valid Until 3/23/2021)](https://join.slack.com/t/seattle-devops/shared_invite/zt-lg6w792s-sowxkaLeSBUm7w79wO4wzg)


## [ Next Month: April 28th, 2021 at 4pm - 5:30pm PT](https://seattledevops.net/posts/upcoming_events/20210428/) 
Mandy Riso - How to build once, deploy many using Azure DevOps multi-stage pipelines
Kat Cosgrove - CI/CD Through the Ages



